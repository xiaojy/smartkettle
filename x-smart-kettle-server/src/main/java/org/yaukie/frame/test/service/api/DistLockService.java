package org.yaukie.frame.test.service.api;

import org.yaukie.base.core.service.Service;
import org.yaukie.frame.test.model.DistLock;
import org.yaukie.frame.test.model.DistLockExample;

import java.util.List;

/**
* @author: yuenbin
* @create: 2022/12/26 16/20/318
**/
public interface DistLockService extends Service<DistLock,DistLockExample> {

    int doSth();


}
